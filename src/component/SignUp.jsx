import React from "react";
import signup from "../assets/image/product-sample@1x.webp";
import uparrow from "../assets/image/up-arrow-outbox-svgrepo-com.png";
import downarrow from "../assets/image/down-arrow-backup-2-svgrepo-com.png";
import { useState } from "react";
import "../assets/css/style.css"
const SignUp = () => {
  const [show, setShow] = useState(false);
  const [submit , setSubmit] = useState(false)
  const handlechange = () => {
    setShow(!show);
  };
  const [formData, setFormData] = useState({
    email: "",
    password: ""
  });

  const Changehandler = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handlesubmit = ()=>{
    setSubmit(true)
  }
  return (
    <div className="h-screen relative">
      <div className="grid grid-cols-2 gap-2 ">
      <div className="hidden md:flex justify-center items-center h-[100vh] bg-[black] fixed w-[45%] ">
          <div className="flex flex-col justify-center items-center ">
            <h1 className="text-[36px] text-white font-normal text-center mb-[35px] mt-[30px]">
              Sign up <br /> and come on in
            </h1>
            <img src={signup} alt="" className="max-w-[366px] " />
            <p className="text-white text-[14px] mt-[120px] pb-0">@ Typeform</p>
          </div>
        </div>
        <div className="rounded-xl md:absolute md:z-10 md:right-0 md:w-[57%] px-8 py-5 bg-white min-h-[100vh] w-[100vw]">
          <div className="flex justify-between">
            <div className="flex items-center">
              <label htmlFor="">
                <svg
                  height="17"
                  viewBox="0 0 20 20"
                  width="17"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10S15.5 0 10 0zM9 17.9C5.1 17.4 2 14 2 10c0-.6.1-1.2.2-1.8L7 13v1c0 1.1.9 2 2 2v1.9zm6.9-2.5c-.3-.8-1-1.4-1.9-1.4h-1v-3c0-.6-.4-1-1-1H6V8h2c.6 0 1-.4 1-1V5h2c1.1 0 2-.9 2-2v-.4c2.9 1.2 5 4.1 5 7.4 0 2.1-.8 4-2.1 5.4z"
                    fill="#5E5E5E"
                    fill-rule="evenodd"
                  ></path>
                </svg>
              </label>
              <select>
                <option value="english">English</option>
                <option value="spanish">Spanish</option>
              </select>
            </div>
            <div className="flex gap-2 items-center">
              <p className="text-[14px]">Already have an account?</p>
              <button className="border border-black rounded-md pt-1 h-[25px] font-medium text-[12px] w-[55px] text-center hover:text-gray-500">
                Log in
              </button>
            </div>
          </div>
          <div className={`flex justify-center items-center ${!show ? 'pt-[100px] pb-[50px]' : 'pt-[150px]'}`}>
            <div className="overflow-y-auto">
            <div className="flex flex-col lg:max-w-[550px]">
              <div className="flex gap-5 justify-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="33"
                  height="22"
                  fill="none"
                >
                  <path
                    fill="currentColor"
                    fill-rule="evenodd"
                    d="M0 5.34C0 1.82 1.39 0 3.72 0c2.34 0 3.73 1.82 3.73 5.34V16c0 3.52-1.4 5.34-3.73 5.34S0 19.52 0 16V5.34ZM25.08 0h-7.7c-6.9 0-7.44 2.98-7.44 6.96l-.01 7.42c0 4.14.52 6.96 7.48 6.96h7.67c6.92 0 7.43-2.97 7.43-6.94V6.97c0-3.99-.53-6.97-7.43-6.97Z"
                    clip-rule="evenodd"
                  ></path>
                </svg>
                <span class="sc-da9128ae-0 bmXElW">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="108"
                    height="24"
                    fill="none"
                    class="sc-da9128ae-1 bOQHzD"
                  >
                    <path
                      fill="currentColor"
                      d="M69.87 16.61c-2.22 0-3.37-1.83-3.37-4.08s1.13-3.99 3.37-3.99c2.29 0 3.37 1.82 3.37 3.99-.02 2.29-1.16 4.08-3.37 4.08ZM48.1 8.54c1.3 0 1.84.76 1.84 1.42 0 1.6-1.62 2.29-5.01 2.39 0-1.97 1.12-3.8 3.17-3.8Zm-14.44 8.07c-2.1 0-2.99-1.71-2.99-4.08 0-2.35.9-3.99 3-3.99 2.12 0 3.12 1.7 3.12 3.99 0 2.39-1.04 4.08-3.13 4.08Zm-17.8-10.4h-3.3l5.46 12.51c-1.04 2.31-1.6 2.89-2.32 2.89-.77 0-1.49-.62-2-1.16l-1.45 1.92a5.14 5.14 0 0 0 3.7 1.63c1.73 0 3.05-1 3.82-2.79l6.3-15.02h-3.24l-3.28 8.97-3.7-8.95Zm87.1 2.33c1.6 0 1.92 1.1 1.92 3.67v6.75h2.85v-8.52c0-3.07-2.1-4.4-4.05-4.4-1.73 0-3.31 1.07-4.2 3.06a4.01 4.01 0 0 0-3.96-3.07c-1.63 0-3.25 1.04-4.13 2.97V6.21h-2.85v12.73h2.85V13.5c0-2.74 1.44-4.96 3.4-4.96 1.6 0 1.9 1.1 1.9 3.67v6.75h2.86l-.02-5.46c0-2.74 1.46-4.96 3.42-4.96ZM80.14 6.21h-1.36v12.73h2.85v-4.88c0-3.09 1.36-5.18 3.39-5.18.52 0 .96.02 1.44.22l.44-3c-.36-.05-.68-.09-1-.09-2 0-3.45 1.38-4.29 3.15V6.21h-1.47Zm-10.28-.2c-3.77 0-6.31 2.87-6.31 6.5 0 3.76 2.58 6.57 6.31 6.57 3.8 0 6.38-2.89 6.38-6.57C76.23 8.86 73.6 6 69.87 6Zm-21.61 10.6a2.98 2.98 0 0 1-3.05-2.29c3.77-.16 7.46-1.08 7.46-4.4 0-1.91-1.89-3.89-4.6-3.89-3.64 0-6.1 2.97-6.1 6.5 0 3.68 2.42 6.57 6.05 6.57a6.62 6.62 0 0 0 5.39-2.49l-1.38-1.87c-1.47 1.5-2.37 1.87-3.77 1.87ZM34.22 6.01c-1.44 0-2.89.84-3.45 2.16V6.2h-2.84v17.73h2.84v-6.33c.6.91 1.99 1.51 3.21 1.51 3.8 0 5.8-2.8 5.8-6.6-.02-3.74-1.99-6.5-5.56-6.5Zm-19.97-4.9H.82v2.77h5.25v15.06h2.99V3.88h5.2V1.12Zm42.33 5.1h-1.7v2.55h1.7v10.18h2.85V8.76h2.76V6.21h-2.76V4.22c0-1.27.52-1.71 1.7-1.71.44 0 .84.12 1.38.3l.65-2.4A5.44 5.44 0 0 0 60.9 0c-2.73 0-4.33 1.63-4.33 4.46v1.75Z"
                    ></path>
                  </svg>
                </span>
              </div>
              <div className="text-center">
                <h2 className="text-[24px] font-extralight text-[rgb(94, 94, 94)] mt-5">
                  Get better data with conversational forms, surveys, quizzes &
                  more
                </h2>
              </div>
              <div className="flex flex-col gap-5 justify-center items-center">
                <div>
                  <input
                    type="email"
                    id="email"
                    name="email"
                    autocomplete="email"
                    required
                    class="mt-1 block w-[255px] h-[42px] px-3 py-2 border border-[1px solid rgb(194, 194, 193)] rounded-md shadow-sm focus:outline-none sm:text-sm focus:border-black"
                    placeholder="Email"
                    onChange={Changehandler}
                  />
                 {(formData.email == "" && submit) &&<p className="text-red-500">This field cannot be left blank</p>}
                </div>
                <div>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    autocomplete="current-password"
                    required
                    class="mt-1 block w-[255px] h-[42px]  px-3 py-2 border border-[1px solid rgb(194, 194, 193)] rounded-md shadow-sm focus:outline-none sm:text-sm focus:border-black"
                    placeholder="Password"
                    onChange={Changehandler}

                  />
                  {(formData.password == "" &&submit) && <p className="text-red-500">This field cannot be left blank</p>}

                </div>
                <div class="flex gap-2 items-center w-[255px]">
                  <input
                    type="checkbox"
                    id="agreeCheckbox"
                    class="h-4 w-4 text-blue-600 focus:ring-blue-500 border-gray-300 rounded"
                  />

                  <label for="agreeCheckbox" class="text-sm">
                    I agree to Typeform’s{" "}
                    <a href="#" class="underline">
                      Terms of Service
                    </a>
                    ,{" "}
                    <a href="#" class="underline">
                      Privacy Policy
                    </a>
                    , and{" "}
                    <a href="#" class="underline">
                      Data Processing Agreement
                    </a>
                    .
                  </label>
                </div>
              <div className="w-[255px] ml-7">
                  <div className="flex gap-2 items-center justify-between w-[226px]">
                    <p onClick={handlechange}
>See options</p>
                    {show ? (
                      <img
                        src={uparrow}
                        alt=""
                        className="w-[15px] h-[15px]"
                        onClick={handlechange}
                      />
                    ) : (
                      <img
                        src={downarrow}
                        alt=""
                        className="w-[20px] h-[15px]"
                        onClick={handlechange}
                      />
                    )}
                  </div>
                  { show && 
                  <div class="flex flex-col gap-5">
                    <div className="flex flex-col gap-2">
                      <label htmlFor="" className="font-normal">
                        Get useful tips, inspiration, and offers via
                        e-communication.
                      </label>
                      <div className="flex gap-2">
                      <div className="flex items-center gap-1"><input
                        type="radio"
                        class="form-radio h-5 w-5 text-black"
                        name="communication"
                        value="yes"
                      />
                      <label htmlFor="">Yes</label></div>
                    <div className="flex items-center gap-1">
                    <input
                        type="radio"
                        class="form-radio h-5 w-5 text-black"
                        name="communication"
                        value="no"
                      />
                      <label htmlFor="">No</label>
                    </div>
                      </div>
                    </div>
                    <div className="flex flex-col gap-2">
                      <label htmlFor="" className="font-normal">
                      Tailor Typeform to my needs based on my activity.See Privacy Policy
                      </label>
                      <div className="flex gap-2">
                      <div className="flex items-center gap-1"><input
                        type="radio"
                        class="form-radio h-5 w-5 text-black"
                        name="communication"
                        value="yes"
                      />
                      <label htmlFor="">Yes</label></div>
                    <div className="flex items-center gap-1">
                    <input
                        type="radio"
                        class="form-radio h-5 w-5 text-black"
                        name="communication"
                        value="no"
                      />
                      <label htmlFor="">No</label>
                    </div>
                      </div>
                    </div>
                    <div className="flex flex-col gap-2">
                      <label htmlFor="" className="font-normal">
                      Enrich my data with select third parties for more relevant content.
                      <br/>
                    <a className="text-gray-500" href="">See Privacy Policy</a>
                      </label>
                      <div className="flex gap-2">
                      <div className="flex items-center gap-1"><input
                        type="radio"
                        class="form-radio h-5 w-5 text-black"
                        name="communication"
                        value="yes"
                      />
                      <label htmlFor="">Yes</label></div>
                    <div className="flex items-center gap-1">
                    <input
                        type="radio"
                        class="form-radio h-5 w-5 text-black"
                        name="communication"
                        value="no"
                      />
                      <label htmlFor="">No</label>
                    </div>
                      </div>
                    </div>
                    <p className="text-gray-500">You can update your preferences in your Profile at any time</p>
                  </div>}
                  <button onClick={handlesubmit} className="bg-black text-white w-[230px] h-[40px] rounded-md mt-5">Create my free account</button>

                </div> 
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
